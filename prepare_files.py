import click
import json
import muda
import numpy as np
import os

from utils import split_file

TMP_SPLIT_FOLDER = './tmp/'

def read_selected_files(selected_files):
	with open(selected_files, 'r') as f:
		return json.load(f)

def get_audio_filename(lab_filename, input_audio_folder):
	return os.path.join(input_audio_folder, os.path.basename(lab_filename).split('.')[0] + '.mp3')


def add_noise(audio_file, noise_folder, output_folder):
	noise_files = [os.path.join(noise_folder, f) for f in os.listdir(noise_folder) if os.path.isfile(os.path.join(noise_folder, f))]

	output_audio_path = os.path.join(output_folder, os.path.basename(audio_file))

	jam_in = muda.load_jam_audio(None, audio_file)
	deformer = muda.deformers.BackgroundNoise(n_samples=1, files=[np.random.choice(noise_files)])
	jam_out = list(deformer.transform(jam_in))[0]

	muda.save(output_audio_path, 'out.jams', jam_out)


@click.command()
@click.option('--selected_files', required=True, help='json file containing description of selected excerpts (output of dataset_selection)')
@click.option('--input_audio_folder', required=True, help='folder containing original audio files')
@click.option('--output_folder', default='out', help='folder where resulting audio excerpts will be saved')
@click.option('--noise_folder', default=None, help='folder with noise files to be added, if none is given, no noise is added')
def prepare_files(selected_files, input_audio_folder, output_folder, noise_folder):
	if not os.path.exists('./tmp'):
		os.mkdir('./tmp')

	add_noise_enabled = noise_folder is not None
	print("Noise Enabled:", add_noise_enabled)
	selected_excerpts = read_selected_files(selected_files)

	for lab_file, splits in selected_excerpts.items():
		print("Processing %s" % lab_file)
		
		audio_file = get_audio_filename(lab_file, input_audio_folder)
		if not add_noise_enabled:
			split_file(audio_file, lab_file, splits, output_folder, output_folder)
		else:
			tmp_audio_files = split_file(audio_file, lab_file, splits, TMP_SPLIT_FOLDER, output_folder)
			for audio_split in tmp_audio_files:
				add_noise(audio_split, noise_folder, output_folder)
    


if __name__ == '__main__':
    prepare_files()