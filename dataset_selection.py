import click
import json

from utils import get_prediction_files, get_prediction_metadata, select_chords_by_class

DEFAULT_COMMON_CHORD_TYPES = ['N', 'maj', 'min']
DEFAULT_MIN_DURATION_SECS = 15


def select_highest_confidence_rare_chords(chord_df, file_metadata, common_chord_types, total_duration_excpected, min_duration):
    rare_chords = chord_df[~chord_df.chord_type.isin(common_chord_types)]
    rare_classes = rare_chords['chord_type'].unique() 
    nb_classes = len(rare_classes)
    duration_per_class = total_duration_excpected/nb_classes
    classes_duration = {}
    selected_files = {}
    
    for c in rare_classes:
        print("Running for %s" % c)
        select_chords_by_class(rare_chords, chord_df, file_metadata, c, classes_duration,
            selected_files, duration_per_class, min_duration)
    
    return selected_files, classes_duration


def read_common_chord_definition(common_chord_definition):
    with open(common_chord_definition, 'r') as f:
        return json.load(f)


def get_common_chord_types(common_chord_definition):
    if common_chord_definition is None:
        return DEFAULT_COMMON_CHORD_TYPES
    else:
        return read_common_chord_definition(common_chord_definition)


def output_results(output_file, selected_files, classes_duration):
    with open(output_file, 'w') as f:
        json.dump(selected_files, f)

    with open('classes_duration.json', 'w') as f:
        json.dump(classes_duration, f)


@click.command()
@click.option('--prediction_folder', required=True, help='location of predicted lab files')
@click.option('--total_duration', required=True, help='total duration in seconds of files to be selected', type=int)
@click.option('--common_chord_definition', default=None, help='optional definition of which chords should be considered as common')
@click.option('--min_duration', default=DEFAULT_MIN_DURATION_SECS, help='minimum duration of excerpt to be selected, default = 15s', type=int)
@click.option('--output_file', default='selected_files.json', help='output file to be used to save the selected excerpts, default = selected_files.json')
def dataset_selection(prediction_folder, total_duration, common_chord_definition, min_duration, output_file):
    common_chord_types = get_common_chord_types(common_chord_definition)
    prediction_files = get_prediction_files(prediction_folder)

    file_metadata, chord_metadata = get_prediction_metadata(prediction_files)
    selected_files, classes_duration = select_highest_confidence_rare_chords(
        chord_metadata, file_metadata, common_chord_types,
        total_duration, min_duration)

    output_results(output_file, selected_files, classes_duration)


if __name__ == '__main__':
    dataset_selection()