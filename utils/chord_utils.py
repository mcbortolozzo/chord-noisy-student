
def update_selected_files(selected_files, filename, seq_start, seq_end):
    if filename not in selected_files:
        selected_files[filename] = [[seq_start, seq_end]]
        return (seq_start, seq_end)
    else:
        file_sequences = selected_files[filename]
        seq_idx = 0
        prev_seq = None
        while seq_idx < len(file_sequences):
            current_sequence = file_sequences[seq_idx]
            if seq_start >= current_sequence[0] and seq_end <= current_sequence[1]:
                return None
            elif seq_end < current_sequence[0] and (prev_seq is None or seq_start > prev_seq[1]):
                file_sequences.insert(seq_idx, [seq_start, seq_end])
                return seq_start, seq_end
            elif seq_end < current_sequence[0] and seq_start <= prev_seq[1]:
                new_interval = (prev_seq[1], seq_end)
                file_sequences[seq_idx - 1][1] = seq_end
                return new_interval
            elif (prev_seq is None or seq_start > prev_seq[1]) and seq_start < current_sequence[0] and seq_end >= current_sequence[0]:
                new_interval = (seq_start, current_sequence[0])
                file_sequences[seq_idx][0] = seq_start
                return new_interval
            elif prev_seq is not None and seq_start <= prev_seq[1] and seq_end >= current_sequence[0]:
                new_interval = (prev_seq[1], current_sequence[0])
                file_sequences[seq_idx-1][1] = file_sequences[seq_idx][1]
                file_sequences.pop(seq_idx)
                return new_interval
            prev_seq = current_sequence
            seq_idx += 1
        
        if seq_start <= file_sequences[-1][1]:
            new_interval = (file_sequences[-1][1], seq_end)
            file_sequences[-1][1] = seq_end
            return new_interval
        else:
            file_sequences.append([seq_start, seq_end])
            return seq_start, seq_end               
                                                    


def add_new_chord(chord_entry, file_metadata, selected_files, seq_duration):
    chord_duration = chord_entry['duration']
    remaining_sequence = seq_duration - chord_duration
    seq_start = max(chord_entry['start']-remaining_sequence/2, 0)
    seq_end = min(seq_duration + seq_start, file_metadata['duration'])
    
    inserted_interval = update_selected_files(selected_files, chord_entry['filename'], seq_start, seq_end)
    return inserted_interval
    


def update_classes_duration(filename, chord_metadata, inserted_intervals, classes_duration):
    interval_start, interval_end = inserted_intervals
    file_chords = chord_metadata[chord_metadata['filename'] == filename].sort_values('start')
    interval_chords = file_chords[((file_chords['start'] >= interval_start) & (file_chords['start'] <= interval_end)) | ((file_chords['end'] >= interval_start) & (file_chords['end'] <= interval_end))]
    for _, chord in interval_chords.iterrows():
        chord_type = chord['chord_type']
        chord_start = max(chord['start'], interval_start)
        chord_end = min(chord['end'], interval_end)
        chord_interval = chord_end - chord_start
        if chord_type not in classes_duration:
            classes_duration[chord_type] = 0
        
        classes_duration[chord_type] += chord_interval
    
    

def select_chords_by_class(df, all_chords, file_metadata, chord_type, classes_duration, selected_files, duration_per_class, seq_duration):
    available_chords = df[(df['duration'] > 1) & (df['chord_type'] == chord_type)]
    sorted_chords = available_chords.sort_values('confidence', ascending=False)
    
    total_inserted_duration = 0
    for _,entry in sorted_chords.iterrows():
        if(len(file_metadata[file_metadata['filename'] == entry['filename']]) == 0):
            continue
        entry_file = file_metadata[file_metadata['filename'] == entry['filename']].iloc[0]
        inserted_intervals = add_new_chord(entry, entry_file, selected_files, seq_duration)

        if inserted_intervals is not None:
            total_inserted_duration += inserted_intervals[1] - inserted_intervals[0]
            update_classes_duration(entry_file['filename'], all_chords, inserted_intervals, classes_duration)

        if chord_type in classes_duration and total_inserted_duration > duration_per_class:
            break