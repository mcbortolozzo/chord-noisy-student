import json
import argparse
import os
import pandas as pd
import numpy as np

from pydub import AudioSegment

def load_lab_file(filename):
    intervals = []
    labels = []
    confidences = []
    with open(filename, "r") as f:
        for line in f.readlines():
            start, end, label, confidence = line.split()
            intervals.append([float(start), float(end)])
            labels.append(label)
            confidences.append(float(confidence))
            
    return np.array([np.array(x) for x in intervals]), np.asarray(labels), np.asarray(confidences)

def write_lab_file(filename, content):
	with open(filename, 'w') as f:
		for intervals, label, confidence in content:
			output = "%f %f %s %f\n" % (intervals[0], intervals[1], label, confidence)
			f.write(output)

def split_audio_files(audio_file, splits):
	sound = AudioSegment.from_mp3(audio_file)

	sound_segments = []
	for s in splits:
		start_millis = s[0]*1000
		end_millis = s[1]*1000
		sound_segments.append(sound[start_millis:end_millis])

	return sound_segments

def get_lab_crop(lab_data, split_start, split_end):
	lab_crop = []
	for intervals, label, confidence in zip(*lab_data):
		start, end = intervals
		if (start >= split_start and start <= split_end) or (end <= split_end and end >= split_start):
			new_start = max(start, split_start) - split_start
			new_end = min(end, split_end) - split_start
			lab_crop.append([[new_start, new_end], label, confidence])

	return lab_crop

def split_lab_files(lab_file, splits):
	lab_data = load_lab_file(lab_file)

	lab_segments = []
	for split_start, split_end in splits:
		lab_segments.append(get_lab_crop(lab_data, split_start, split_end))

	return lab_segments



def split_file(audio_file, lab_file, splits, audio_dest, lab_dest):
	filename = os.path.basename(audio_file).split('.')[0]
	audio_splits = split_audio_files(audio_file, splits)
	lab_splits = split_lab_files(lab_file, splits)
	
	assert len(audio_splits) == len(lab_splits)

	audio_results = []
	for i in range(len(audio_splits)):
		output_file = filename + '_' + str(i)
		audio_out_file = os.path.join(audio_dest, output_file + '.wav')
		audio_results.append(audio_out_file)
		audio_splits[i].export(audio_out_file, format='wav')
		write_lab_file(os.path.join(lab_dest, output_file + '.lab'), lab_splits[i])

	return audio_results