import mir_eval

import numpy as np
import pandas as pd

from os import listdir
from os.path import isfile, join


def load_lab_file(filename):
    intervals = []
    labels = []
    confidences = []
    with open(filename, "r") as f:
        for line in f.readlines():
            start, end, label, confidence = line.split()
            intervals.append([float(start), float(end)])
            labels.append(label)
            confidences.append(float(confidence))
            
    return np.array([np.array(x) for x in intervals]), np.asarray(labels), np.asarray(confidences)


def update_chord_metadata(chord_metadata, filename, intervals, labels, confidences):
    for i in range(len(labels)):
        chord_metadata['confidence'].append(confidences[i])
        chord_metadata['filename'].append(filename)
        chord_metadata['start'].append(intervals[i][0])
        chord_metadata['end'].append(intervals[i][1])
        chord_metadata['duration'].append(intervals[i][1] - intervals[i][0])
        
        root, modifier, quality, bass = mir_eval.chord.split(labels[i])
        if root == 'N':
            modifier = 'N'
        chord_metadata['chord_type'].append(modifier)

    return chord_metadata


def update_classes(class_distribution, duration, labels):
    for i in range(len(labels)):
        root, modifier, quality, bass = mir_eval.chord.split(labels[i])
        if root == 'N':
            modifier = 'N'
        
        if modifier not in class_distribution:
            class_distribution[modifier] = 0
        class_distribution[modifier] += duration[i]
        
    return class_distribution


def get_class_distribution(files):
    class_distribution = {}
    for f in files:
        intervals, labels, confidences = load_lab_file(f)
        interval_durations = intervals[:, 1] - intervals[:,0]
        class_distribution = update_classes(class_distribution, interval_durations, labels)
    
    return class_distribution  


def update_file_metadata(file_metadata, chord_metadata, filename, classes):
    intervals, labels, confidences = load_lab_file(filename)
    chord_metadata = update_chord_metadata(chord_metadata, filename, intervals, labels, confidences)

    file_duration = intervals[-1][1]
    file_metadata['duration'].append(file_duration)
    durations = intervals[:,1] - intervals[:, 0]
    class_distribution = {}
    update_classes(class_distribution, durations, labels)
    for c in classes:
        if c in class_distribution:
            file_metadata[c].append(class_distribution[c])
        else:
            file_metadata[c].append(0)
    
    file_metadata['confidence'].append(np.sum(confidences*(durations))/file_duration)
    
    file_metadata['filename'].append(filename)    
    
    return file_metadata, chord_metadata


def build_file_metadata(files):
    file_metadata = {}
    file_metadata['confidence'] = []
    file_metadata['filename'] = []
    file_metadata['duration'] = []
    classes = get_class_distribution(files).keys()
    for c in classes:
        file_metadata[c] = []

    return file_metadata, classes


def build_chord_metadata():
    chord_metadata = {}
    chord_metadata['confidence'] = []
    chord_metadata['filename'] = []
    chord_metadata['start'] = []
    chord_metadata['end'] = []
    chord_metadata['duration'] = []
    chord_metadata['chord_type'] = []

    return chord_metadata


def get_prediction_metadata(files):
    file_metadata, classes = build_file_metadata(files)
    chord_metadata = build_chord_metadata()
        
    for f in files:
        file_metadata, chord_metadata = update_file_metadata(file_metadata, chord_metadata, f, classes)     
        
    return pd.DataFrame.from_dict(file_metadata), pd.DataFrame.from_dict(chord_metadata)

def get_prediction_files(folder):
    return [join(folder, f) for f in listdir(folder) if isfile(join(folder, f))]