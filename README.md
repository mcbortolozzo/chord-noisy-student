# Chord Recognition Noisy Student

This repository contains the implementation for an adapted NoisyStudent algorithm, on the domain of Automatic Chord Recognition (ACR).

## Installation 

Install the dependencies via

```
pip install -r requirements.txt
```

## Usage

Selecting the audio/label for the NoisyStudent iteration:

```
python dataset_selection.py --prediction_folder <path to lab files folder> --total_duration <total desired duration of selected excerpts>
```

This generates an json output file with the specs for the audio excerpts split.

Important: all .lab are presumed to have a fourth column after the on/offset and the label, which provides the average confidence predicted for that label.



Extracting the excerpts from the original audio, adding noise, if desired:


```
python prepare_files.py --selected_files <json spec of selected excerpts> --input_audio_folder <original audio's folder> --output_folder <desired output folder for audio/lab excerpts> --noise_folder <optional folder with noise audio files>
```

Additional parameters can be accessed through the help menu of the commands.

